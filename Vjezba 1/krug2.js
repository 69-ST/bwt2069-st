
class Krug{
 
    constructor(x,y,precnik){
        this.x=x;
        this.y=y;
        this.precnik = precnik;
    }
  
    get povrsina() {
        return Math.pow(this.precnik/2,2)*Math.PI;
    }
  
    get obim() {
        return this.precnik*Math.PI;
    }
  
    presjek(krug2) {
       return Math.pow(krug2.precnik+this.precnik,2) >= (Math.pow((krug2.x-this.x),2) + Math.pow((krug2.y-this.y),2));
    }
 }

 class Papir{
    constructor(h,w){
        this.h = h;
        this.w = w;
        this.k  = [];
    }
    get duzinaNiza(){
        return this.k.length;
    }
    dodajKrug(krug){
        let povrsina = this.h*this.w;
        for(let element of this.k){
            povrsina = povrsina - element.povrsina;
        }
        if(krug.povrsina<=povrsina){
            this.k.push(krug); 
        }else{
            console.log("Krug sa centrom " + krug.x + "," + krug.y + " i prečnikom " + krug.precnik + " ne može stati na papir");
        }
    }

    ispisiKrugove(){
        let string="";
        for(let e of this.k){
            let s = `Krug (${e.x},${e.y},${e.precnik}) sa površinom ${e.povrsina}`;
            string+= (s+" ");
            console.log(s);   
        }
        return string;   
    }

 }