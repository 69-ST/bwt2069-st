let assert = chai.assert;
describe('Krug', function() {
 describe('#povrsina kruga', function() {
   it('treba vratiti PI kada je prečnik kruga 2', function() {
       const k1= new Krug(1,1,2);
       assert.equal(k1.povrsina, Math.PI);
   });
   it('treba vratiti 0 kada je prečnik kruga 0', function() {
     const k1= new Krug(1,1,0);
     assert.equal(k1.povrsina, 0);
   });
   it('treba vratiti 4PI kada je prečnik kruga 4', function() {
     const k1= new Krug(1,1,4);
     assert.equal(k1.povrsina, 4*Math.PI);
   });
 });
 describe('#obim kruga', function() {
  it('treba vratiti PI kada je prečnik kruga 1', function() {
      const k1= new Krug(1,1,1);
      assert.equal(k1.obim, Math.PI);
  });
  it('treba vratiti 0 kada je prečnik kruga 0', function() {
    const k1= new Krug(1,1,0);
    assert.equal(k1.obim, 0);
  });
  it('treba vratiti 4PI kada je prečnik kruga 4', function() {
    const k1= new Krug(1,1,4);
    assert.equal(k1.obim, 4*Math.PI);
  });
});
describe('#presjek kruga', function() {
  it('presjecaju se', function() {
     const k1= new Krug(1,1,5);
     const k2 = new Krug(2,5,2)
     assert.equal(k2.presjek(k1),true);
     });
  it(' presjecaju se jer su isti', function() {
         const k1 = new Krug(20,4,2);
         const k2 = new Krug(20,4,2)
         assert.equal(k1.presjek(k2),true);
     });
     it('ne presjecaju se', function() {
         const k1 = new Krug(5,5,0);
         const k2 = new Krug(1,1,0)
         assert.equal(k1.presjek(k2),false);
     });
 });
});
describe('Papir', function() {
  describe('#dodaj Krug', function() {
    it('treba dodati 2 kruga', function() {
        const k1= new Krug(1,1,2);
        const k2= new Krug(2,2,2);
        const p = new Papir(5,5);
        p.dodajKrug(k1);
        p.dodajKrug(k2);
        assert.equal(p.duzinaNiza, 2);
        
    });
    it('treba izbaciti poruku', function() {
      const k1= new Krug(1,1,2);
        const p = new Papir(1,2);
        p.dodajKrug(k1);
        assert.equal(p.duzinaNiza, 0);
    });
    it('treba jedan dodati, drugi ne', function() {
      const k1= new Krug(1,1,2);
      const p = new Papir(2,3);
      p.dodajKrug(k1);
      assert.equal(p.duzinaNiza,1);
      p.dodajKrug(k1);
      assert.equal(p.duzinaNiza,1);
    });
  });
  describe('#ispisi Krug', function() {
    it('nista se ne ispisuje', function() {
      const k1= new Krug(1,1,2);
        const p = new Papir(1,2);
        p.dodajKrug(k1);
        assert.equal(p.ispisiKrugove(), "");
    });
    
  });
  
 });
